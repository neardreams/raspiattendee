import MFRC522
import signal
import RPi.GPIO as GPIO
import MySQLdb
import time
import atexit
continue_reading = True
db_not_ready = True
MIFAREReader = MFRC522.MFRC522()

GPIO.setmode(GPIO.BOARD)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(36,GPIO.OUT)
GPIO.setup(38,GPIO.OUT)
GPIO.setup(40,GPIO.OUT)

while db_not_ready:
    try:
        dbtest = MySQLdb.connect("localhost","root","19850305","attendee")
        cursortest=dbtest.cursor()
        cursortest.execute("SELECT VERSION()")
        results = cursortest.fetchone()
        ver = results[0]
        if (ver is None):
            db_not_ready = True
        else:
            db_not_ready = False
            time.sleep(2)
        dbtest.close()
    except:
        db_not_ready = True
    time.sleep(1)
    
GPIO.output(40,0)

def end_read(signal, frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()
    db.close()

while continue_reading:
  (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
  if status == MIFAREReader.MI_OK:
    print "Card detected"
  (status,backData) = MIFAREReader.MFRC522_Anticoll()
  if status == MIFAREReader.MI_OK:
    print "Card read UID: "+str(backData[0])+","+str(backData[1])+","+str(backData[2])+","+str(backData[3])+","+str(backData[4])

    GPIO.output(18,1)
    GPIO.output(40,1)
    time.sleep(.05)
    GPIO.output(18,0)
    GPIO.output(40,0)
    time.sleep(.05)
    GPIO.output(18,1)
    GPIO.output(40,1)
    time.sleep(.05)
    GPIO.output(18,0)
    GPIO.output(40,0)
    time.sleep(.05)
    GPIO.output(18,1)
    GPIO.output(40,1)
    time.sleep(.05)
    GPIO.output(18,0)
    GPIO.output(40,0)
    db = MySQLdb.connect("localhost","root","19850305","attendee")
    cursor = db.cursor()
    cursor.execute('''INSERT into attend_logs (card_id)values (%s)''',(str(backData[0])+","+str(backData[1])+","+str(backData[2])+","+str(backData[3])+","+str(backData[4])))
    print backData
    db.commit()
    time.sleep(.5)
